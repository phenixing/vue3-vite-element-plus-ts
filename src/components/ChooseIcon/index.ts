import { App } from 'vue'
import ChooseIconComponent from './src/index.vue'

export default {
  install(app: App) {
    app.component('ChooseIconComponent', ChooseIconComponent)
  }
}
