import { App } from 'vue'
import ChooseAreaComponent from './ChooseArea'
import ChooseIconComponent from './ChooseIcon'

const components = [
  ChooseAreaComponent,
  ChooseIconComponent
]

export default {
  install(app: App) {
    components.map(item => app.use(item))
  }
}
