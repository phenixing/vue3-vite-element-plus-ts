import { App } from 'vue'
import ChooseAreaComponent from './src/index.vue'

export default {
  install(app: App) {
    app.component('ChooseAreaComponent', ChooseAreaComponent)
  }
}
