import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

// 引入ElementPlus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import mUI from './components/index'

import { toLine } from './utils/index'

const app = createApp(App)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(`el-icon-${toLine(key)}`, component)
}

app
  .use(router)
  .use(ElementPlus)
  .use(mUI)
  .mount('#app')
